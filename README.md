EXT:fullcalendar
==================

Uses fullcalendar.io to generate a skin-able calendar with different views (month, week, day, week list, day list etc.) from EXT:calendarize and/or one or more google calendar feeds.

Additionally, it provides a filter system to show only specific categories of data.

Restriction of calendar categories to specific user groups is also possible.
