<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Domain\Model;

/**
 * Class Category
 */
class Category extends \TYPO3\CMS\Extbase\Domain\Model\Category
{
    /**
     * Frontend user restriction
     *
     * @var string
     */
    protected $fullcalendarFeGroupRestriction;

    /**
     * Color
     *
     * @var string
     */
    protected $color;

    /**
     * @return string
     */
    public function getFullcalendarFeGroupRestriction(): ?string
    {
        return $this->fullcalendarFeGroupRestriction;
    }

    /**
     * @param string $fullcalendarFeGroupRestriction
     */
    public function setFullcalendarFeGroupRestriction(string $fullcalendarFeGroupRestriction): void
    {
        $this->fullcalendarFeGroupRestriction = $fullcalendarFeGroupRestriction;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }
}
