<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Class SysFileReference.
 */
class CategoryFilter extends AbstractEntity
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var ObjectStorage<Category>
     */
    protected $categories;

    public function __construct()
    {
        $this->categories = new ObjectStorage();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return CategoryFilter
     */
    public function setTitle($title): CategoryFilter
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return ObjectStorage
     */
    public function getCategories(): ObjectStorage
    {
        return $this->categories;
    }

    /**
     * @param ObjectStorage<Category> $categories
     * @return CategoryFilter
     */
    public function setCategories($categories): CategoryFilter
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @param Category $category
     */
    public function attachCategory($category): void
    {
        $this->categories->attach($category);
    }

    /**
     * @param Category $category
     */
    public function detachCategory($category): void
    {
        $this->categories->detach($category);
    }

    public function removeAllCategories(): void
    {
        $this->categories = new ObjectStorage();
    }

}
