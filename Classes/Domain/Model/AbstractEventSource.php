<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

abstract class AbstractEventSource extends AbstractEntity
{
    /**
     * @var string
     */
    protected $id = '';

    /**
     * @var string
     */
    protected $color = '';

    /**
     * @var string
     */
    protected $backgroundColor = '';

    /**
     * @var string
     */
    protected $borderColor = '';

    /**
     * @var string
     */
    protected $textColor = '';

    /**
     * @var string
     */
    protected $className = '';

    /**
     * @var bool
     */
    protected $editable = false;

    /**
     * @var bool
     */
    protected $startEditable = false;

    /**
     * @var bool
     */
    protected $durationEditable = false;

    /**
     * @var bool
     */
    protected $resourceEditable = false;

    /**
     * @var string
     */
    protected $rendering = '';

    /**
     * @var bool
     */
    protected $overlap = true;

    /**
     * @var bool
     */
    protected $allDayDefault = false;

    /**
     * @var string
     */
    protected $eventDataTransform = '';

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return AbstractEventSource
     */
    public function setId(string $id): AbstractEventSource
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return AbstractEventSource
     */
    public function setColor(string $color): AbstractEventSource
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return string
     */
    public function getBackgroundColor(): string
    {
        return $this->backgroundColor;
    }

    /**
     * @param string $backgroundColor
     * @return AbstractEventSource
     */
    public function setBackgroundColor(string $backgroundColor): AbstractEventSource
    {
        $this->backgroundColor = $backgroundColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getBorderColor(): string
    {
        return $this->borderColor;
    }

    /**
     * @param string $borderColor
     * @return AbstractEventSource
     */
    public function setBorderColor(string $borderColor): AbstractEventSource
    {
        $this->borderColor = $borderColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getTextColor(): string
    {
        return $this->textColor;
    }

    /**
     * @param string $textColor
     * @return AbstractEventSource
     */
    public function setTextColor(string $textColor): AbstractEventSource
    {
        $this->textColor = $textColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @param string $className
     * @return AbstractEventSource
     */
    public function setClassName(string $className): AbstractEventSource
    {
        $this->className = $className;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->editable;
    }

    /**
     * @param bool $editable
     * @return AbstractEventSource
     */
    public function setEditable(bool $editable): AbstractEventSource
    {
        $this->editable = $editable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStartEditable(): bool
    {
        return $this->startEditable;
    }

    /**
     * @param bool $startEditable
     * @return AbstractEventSource
     */
    public function setStartEditable(bool $startEditable): AbstractEventSource
    {
        $this->startEditable = $startEditable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDurationEditable(): bool
    {
        return $this->durationEditable;
    }

    /**
     * @param bool $durationEditable
     * @return AbstractEventSource
     */
    public function setDurationEditable(bool $durationEditable): AbstractEventSource
    {
        $this->durationEditable = $durationEditable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isResourceEditable(): bool
    {
        return $this->resourceEditable;
    }

    /**
     * @param bool $resourceEditable
     * @return AbstractEventSource
     */
    public function setResourceEditable(bool $resourceEditable): AbstractEventSource
    {
        $this->resourceEditable = $resourceEditable;
        return $this;
    }

    /**
     * @return string
     */
    public function getRendering(): string
    {
        return $this->rendering;
    }

    /**
     * @param string $rendering
     * @return AbstractEventSource
     */
    public function setRendering(string $rendering): AbstractEventSource
    {
        $this->rendering = $rendering;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOverlap(): bool
    {
        return $this->overlap;
    }

    /**
     * @param bool $overlap
     * @return AbstractEventSource
     */
    public function setOverlap(bool $overlap): AbstractEventSource
    {
        $this->overlap = $overlap;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllDayDefault(): bool
    {
        return $this->allDayDefault;
    }

    /**
     * @param bool $allDayDefault
     * @return AbstractEventSource
     */
    public function setAllDayDefault(bool $allDayDefault): AbstractEventSource
    {
        $this->allDayDefault = $allDayDefault;
        return $this;
    }

    /**
     * @return string
     */
    public function getEventDataTransform(): string
    {
        return $this->eventDataTransform;
    }

    /**
     * @param string $eventDataTransform
     * @return AbstractEventSource
     */
    public function setEventDataTransform(string $eventDataTransform): AbstractEventSource
    {
        $this->eventDataTransform = $eventDataTransform;
        return $this;
    }

}
