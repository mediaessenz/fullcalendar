<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Domain\Model;

class GoogleCalendarFeedEventSource extends AbstractEventSource
{
    /**
     * @var string
     */
    protected $googleCalendarApiKey = '';

    /**
     * @var string
     */
    protected $googleCalendarId = '';

    /**
     * @return string
     */
    public function getGoogleCalendarApiKey(): string
    {
        return $this->googleCalendarApiKey;
    }

    /**
     * @param string $googleCalendarApiKey
     * @return GoogleCalendarFeedEventSource
     */
    public function setGoogleCalendarApiKey(string $googleCalendarApiKey): GoogleCalendarFeedEventSource
    {
        $this->googleCalendarApiKey = $googleCalendarApiKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleCalendarId(): string
    {
        return $this->googleCalendarId;
    }

    /**
     * @param string $googleCalendarId
     * @return GoogleCalendarFeedEventSource
     */
    public function setGoogleCalendarId(string $googleCalendarId): GoogleCalendarFeedEventSource
    {
        $this->googleCalendarId = $googleCalendarId;
        return $this;
    }

}
