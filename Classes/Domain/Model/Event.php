<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class Event
 * @package MEDIAESSENZ\Fullcalendar\Domain\Model
 */
class Event extends AbstractEntity
{
    /**
     * @var string
     */
    protected $id = '';

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var bool
     */
    protected $allDay = false;

    /**
     * @var string
     */
    protected $start = '';

    /**
     * @var string
     */
    protected $end = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var string
     */
    protected $detailsUrl = '';

    /**
     * @var string
     */
    protected $bookingUrl = '';

    /**
     * @var string
     */
    protected $className = 'fc-default';

    /**
     * @var bool
     */
    protected $editable = false;

    /**
     * @var bool
     */
    protected $startEditable = false;

    /**
     * @var bool
     */
    protected $durationEditable = false;

    /**
     * @var bool
     */
    protected $resourceEditable = false;

    /**
     * @var string
     */
    protected $rendering = '';

    /**
     * @var bool
     */
    protected $overlap = true;

    /**
     * @var string
     */
    protected $constraint = '';

    /**
     * @var string
     */
    protected $source = '';

    /**
     * @var string
     */
    protected $color = '';

    /**
     * @var string
     */
    protected $backgroundColor = '';

    /**
     * @var string
     */
    protected $borderColor = '';

    /**
     * @var string
     */
    protected $textColor = '';

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var string
     */
    protected $location = '';

    /**
     * @var string
     */
    protected $locationLink = '';

    /**
     * @var string
     */
    protected $organizer = '';

    /**
     * @var string
     */
    protected $organizerLink = '';

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Event
     */
    public function setId(string $id): Event
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Event
     */
    public function setTitle(string $title): Event
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllDay(): bool
    {
        return $this->allDay;
    }

    /**
     * @param bool $allDay
     * @return Event
     */
    public function setAllDay(bool $allDay): Event
    {
        $this->allDay = $allDay;
        return $this;
    }

    /**
     * @return string
     */
    public function getStart(): string
    {
        return $this->start;
    }

    /**
     * @param string $start
     * @return Event
     */
    public function setStart(string $start): Event
    {
        $this->start = $start;
        return $this;
    }

    /**
     * @return string
     */
    public function getEnd(): string
    {
        return $this->end;
    }

    /**
     * @param string $end
     * @return Event
     */
    public function setEnd(string $end): Event
    {
        $this->end = $end;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Event
     */
    public function setUrl(string $url): Event
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @param string $className
     * @return Event
     */
    public function setClassName(string $className): Event
    {
        $this->className = $className;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->editable;
    }

    /**
     * @param bool $editable
     * @return Event
     */
    public function setEditable(bool $editable): Event
    {
        $this->editable = $editable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStartEditable(): bool
    {
        return $this->startEditable;
    }

    /**
     * @param bool $startEditable
     * @return Event
     */
    public function setStartEditable(bool $startEditable): Event
    {
        $this->startEditable = $startEditable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDurationEditable(): bool
    {
        return $this->durationEditable;
    }

    /**
     * @param bool $durationEditable
     * @return Event
     */
    public function setDurationEditable(bool $durationEditable): Event
    {
        $this->durationEditable = $durationEditable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isResourceEditable(): bool
    {
        return $this->resourceEditable;
    }

    /**
     * @param bool $resourceEditable
     * @return Event
     */
    public function setResourceEditable(bool $resourceEditable): Event
    {
        $this->resourceEditable = $resourceEditable;
        return $this;
    }

    /**
     * @return string
     */
    public function getRendering(): string
    {
        return $this->rendering;
    }

    /**
     * @param string $rendering
     * @return Event
     */
    public function setRendering(string $rendering): Event
    {
        $this->rendering = $rendering;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOverlap(): bool
    {
        return $this->overlap;
    }

    /**
     * @param bool $overlap
     * @return Event
     */
    public function setOverlap(bool $overlap): Event
    {
        $this->overlap = $overlap;
        return $this;
    }

    /**
     * @return string
     */
    public function getConstraint(): string
    {
        return $this->constraint;
    }

    /**
     * @param string $constraint
     * @return Event
     */
    public function setConstraint(string $constraint): Event
    {
        $this->constraint = $constraint;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return Event
     */
    public function setSource(string $source): Event
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return Event
     */
    public function setColor(string $color): Event
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return string
     */
    public function getBackgroundColor(): string
    {
        return $this->backgroundColor;
    }

    /**
     * @param string $backgroundColor
     * @return Event
     */
    public function setBackgroundColor(string $backgroundColor): Event
    {
        $this->backgroundColor = $backgroundColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getBorderColor(): string
    {
        return $this->borderColor;
    }

    /**
     * @param string $borderColor
     * @return Event
     */
    public function setBorderColor(string $borderColor): Event
    {
        $this->borderColor = $borderColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getTextColor(): string
    {
        return $this->textColor;
    }

    /**
     * @param string $textColor
     * @return Event
     */
    public function setTextColor(string $textColor): Event
    {
        $this->textColor = $textColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Event
     */
    public function setDescription(string $description): Event
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     * @return Event
     */
    public function setLocation(string $location): Event
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsUrl(): string
    {
        return $this->detailsUrl;
    }

    /**
     * @param string $detailsUrl
     * @return Event
     */
    public function setDetailsUrl(string $detailsUrl): Event
    {
        $this->detailsUrl = $detailsUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getBookingUrl(): string
    {
        return $this->bookingUrl;
    }

    /**
     * @param string $bookingUrl
     * @return Event
     */
    public function setBookingUrl(string $bookingUrl): Event
    {
        $this->bookingUrl = $bookingUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizer(): string
    {
        return $this->organizer;
    }

    /**
     * @param string $organizer
     * @return Event
     */
    public function setOrganizer(string $organizer): Event
    {
        $this->organizer = $organizer;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocationLink(): string
    {
        return $this->locationLink;
    }

    /**
     * @param string $locationLink
     * @return Event
     */
    public function setLocationLink(string $locationLink): Event
    {
        $this->locationLink = $locationLink;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizerLink(): string
    {
        return $this->organizerLink;
    }

    /**
     * @param string $organizerLink
     * @return Event
     */
    public function setOrganizerLink(string $organizerLink): Event
    {
        $this->organizerLink = $organizerLink;
        return $this;
    }

}
