<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Domain\Model;

use TYPO3\CMS\Extbase\Annotation\Validate;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Alexander Grein <alexander.grein@gmail.com>, MEDIA::ESSENZ
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package fullcalendar
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class GoogleCalendarFeed extends AbstractEntity
{

    /**
     * title
     *
     * @var string
     * @Validate("NotEmpty")
     */
    protected $title;

    /**
     * @var string
     * @Validate("NotEmpty")
     */
    protected $googleCalendarApiKey;

    /**
     * @var string
     * @Validate("NotEmpty")
     */
    protected $googleCalendarId;

    /**
     * color
     *
     * @var string
     */
    protected $color;

    /**
     * className
     *
     * @var string
     */
    protected $className;

    /**
     * Categories.
     *
     * @var ObjectStorage<Category>
     */
    protected $categories;


    public function __construct()
    {
        $this->categories = new ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getGoogleCalendarApiKey(): string
    {
        return $this->googleCalendarApiKey;
    }

    /**
     * @param string $googleCalendarApiKey
     */
    public function setGoogleCalendarApiKey($googleCalendarApiKey): void
    {
        $this->googleCalendarApiKey = $googleCalendarApiKey;
    }

    /**
     * @return string
     */
    public function getGoogleCalendarId(): string
    {
        return $this->googleCalendarId;
    }

    /**
     * @param string $googleCalendarId
     */
    public function setGoogleCalendarId($googleCalendarId): void
    {
        $this->googleCalendarId = $googleCalendarId;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * Returns the css
     *
     * @return string $css
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * Sets the css
     *
     * @param string $className
     * @return void
     */
    public function setClassName($className): void
    {
        $this->className = $className;
    }

    /**
     * @return ObjectStorage
     */
    public function getCategories(): ObjectStorage
    {
        return $this->categories;
    }

    /**
     * @param ObjectStorage $categories
     */
    public function setCategories(ObjectStorage $categories): void
    {
        $this->categories = $categories;
    }

    /**
     * Adds a Category.
     *
     * @param Category $category
     */
    public function addCategory(Category $category): void
    {
        $this->categories->attach($category);
    }

    /**
     * Removes a Category.
     *
     * @param Category $categoryToRemove The Category to be removed
     */
    public function removeCategory(Category $categoryToRemove): void
    {
        $this->categories->detach($categoryToRemove);
    }

}
