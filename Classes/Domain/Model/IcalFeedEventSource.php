<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Domain\Model;

class IcalFeedEventSource extends AbstractEventSource
{
    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var string
     */
    protected $format = 'ics';

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return IcalFeedEventSource
     */
    public function setUrl(string $url): IcalFeedEventSource
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param string
     * @return IcalFeedEventSource
     */
    public function setFormat(string $format): IcalFeedEventSource
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }
}
