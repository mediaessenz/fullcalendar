<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Domain\Model;

class JsonFeedEventSource extends AbstractEventSource
{
    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var string
     */
    protected $startParam = 'start';

    /**
     * @var string
     */
    protected $endParam = 'end';

    /**
     * @var string
     */
    protected $timeZoneParam = 'timeZone';

    /**
     * @var string
     */
    protected $type = 'POST';

    /**
     * @var string
     */
    protected $method = 'POST';

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var string
     */
    protected $error = '';

    /**
     * @var bool
     */
    protected $cache = true;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return JsonFeedEventSource
     */
    public function setUrl(string $url): JsonFeedEventSource
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartParam(): string
    {
        return $this->startParam;
    }

    /**
     * @param string $startParam
     * @return JsonFeedEventSource
     */
    public function setStartParam(string $startParam): JsonFeedEventSource
    {
        $this->startParam = $startParam;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndParam(): string
    {
        return $this->endParam;
    }

    /**
     * @param string $endParam
     * @return JsonFeedEventSource
     */
    public function setEndParam(string $endParam): JsonFeedEventSource
    {
        $this->endParam = $endParam;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimeZoneParam(): string
    {
        return $this->timeZoneParam;
    }

    /**
     * @param string $timeZoneParam
     * @return JsonFeedEventSource
     */
    public function setTimeZoneParam(string $timeZoneParam): JsonFeedEventSource
    {
        $this->timeZoneParam = $timeZoneParam;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return JsonFeedEventSource
     */
    public function setType(string $type): JsonFeedEventSource
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return JsonFeedEventSource
     */
    public function setMethod(string $method): JsonFeedEventSource
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return JsonFeedEventSource
     */
    public function setData(array $data): JsonFeedEventSource
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @param string $error
     * @return JsonFeedEventSource
     */
    public function setError(string $error): JsonFeedEventSource
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCache(): bool
    {
        return $this->cache;
    }

    /**
     * @param bool $cache
     * @return JsonFeedEventSource
     */
    public function setCache(bool $cache): JsonFeedEventSource
    {
        $this->cache = $cache;
        return $this;
    }

}
