<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Utility;

use Helhum\TyposcriptRendering\Configuration\ConfigurationBuildingException;
use MEDIAESSENZ\Fullcalendar\Domain\Model\JsonFeedEventSource;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\QueryGenerator;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Information\Typo3Information;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

class CalendarizeUtility
{
    /**
     * @param array $eventSources
     * @param array $settings
     * @param ContentObjectRenderer $contentObject
     * @param UriBuilder $uriBuilder
     * @throws ConfigurationBuildingException
     */
    public static function addEventSources(array &$eventSources, array $settings, ContentObjectRenderer $contentObject, UriBuilder $uriBuilder): void
    {
        $calendarizeStoragePids = GeneralUtility::intExplode(',', $settings['calendarizeStoragePids'], true);

        if (ExtensionManagementUtility::isLoaded('calendarize') && count($calendarizeStoragePids) > 0) {
            if (ExtensionManagementUtility::isLoaded('typoscript_rendering')) {
                $uri = TypoScriptRenderingUtility::generateTypoScriptRenderingUri(
                    $uriBuilder,
                    $contentObject->currentRecord
                );
            } else {
                $uri = $uriBuilder->setArguments([
                    'type' => 1738665709,
                ])->setLinkAccessRestrictedPages(true)
                    ->setCreateAbsoluteUri(true)->build();
                // $uri = '/?typeNum=1738665709';
            }
            // ?typeNum=1738665709
            /** @var JsonFeedEventSource $jsonFeedEventSource */
            $jsonFeedEventSource = GeneralUtility::makeInstance(JsonFeedEventSource::class);
            $eventSources[] = $jsonFeedEventSource
                ->setUrl($uri)
                ->setId(JsonFeedEventSource::class . ':1')
                ->setClassName('fc-default');
        }
    }

    /**
     * @param int $parent
     * @param int $depth
     * @param bool $asArray
     *
     * @return array|string
     */
    public static function getTreePids(int $parent = 0, int $depth = 999, bool $asArray = true): array|string
    {
        if ((new \TYPO3\CMS\Core\Information\Typo3Version())->getMajorVersion() < 12) {
            $childPids = GeneralUtility::makeInstance(QueryGenerator::class)->getTreeList($parent, $depth, 0, 1);
            return $asArray ? GeneralUtility::intExplode(',', $childPids) : $childPids;
        }

        $childPids = GeneralUtility::makeInstance(PageRepository::class)->getDescendantPageIdsRecursive($parent, $depth);

        return $asArray ? $childPids : implode(',', $childPids);
    }

}
