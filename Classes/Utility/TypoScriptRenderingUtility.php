<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Utility;

use Helhum\TyposcriptRendering\Configuration\ConfigurationBuildingException;
use Helhum\TyposcriptRendering\Configuration\RecordRenderingConfigurationBuilder;
use Helhum\TyposcriptRendering\Renderer\RenderingContext;
use Helhum\TyposcriptRendering\Uri\TyposcriptRenderingUri;
use Helhum\TyposcriptRendering\Uri\ViewHelperContext;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;

class TypoScriptRenderingUtility
{
    /**
     * @param string $contextRecord
     * @param string $extensionName
     * @param string $pluginName
     * @param string $controllerName
     * @param string $action
     * @param array $arguments
     * @param string $section
     * @param string $format
     * @param bool $linkAccessRestrictedPages
     * @param array $additionalParams
     * @param bool $absolute
     * @param bool $addQueryString
     * @param array $argumentsToBeExcludedFromQueryString
     * @return string
     * @throws ConfigurationBuildingException
     */
    public static function generateTypoScriptRenderingUri(
        UriBuilder $uriBuilder,
        string $contextRecord = 'tt_content:0',
        string $extensionName = 'Fullcalendar',
        string $pluginName = 'Feed',
        string $controllerName = 'CalendarFeed',
        string $action = 'feed',
        array  $arguments = [],
        string $section = '',
        string $format = '',
        bool   $linkAccessRestrictedPages = false,
        array  $additionalParams = [],
        bool   $absolute = true,
        bool   $addQueryString = false,
        array $argumentsToBeExcludedFromQueryString = []
    ): string
    {
        $pageUid = $GLOBALS['TSFE']->id;
        $addQueryStringMethod = '';
        $renderingConfiguration = self::buildTypoScriptRenderingConfiguration($extensionName, $pluginName, $contextRecord);
        $additionalParams['tx_typoscriptrendering']['context'] = json_encode($renderingConfiguration);

//        /** @var UriBuilder $uriBuilder */
//        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);

        $uriBuilder->reset()
            ->setTargetPageUid((int)$pageUid)
            ->setSection($section)
            ->setFormat($format)
            ->setLinkAccessRestrictedPages($linkAccessRestrictedPages)
            ->setArguments($additionalParams)
            ->setCreateAbsoluteUri($absolute)
            ->setAddQueryString($addQueryString)
            ->setArgumentsToBeExcludedFromQueryString($argumentsToBeExcludedFromQueryString);

        // TYPO3 6.0 compatibility check:
        if (method_exists($uriBuilder, 'setAddQueryStringMethod')) {
            $uriBuilder->setAddQueryStringMethod($addQueryStringMethod);
        }

        $uri = $uriBuilder->uriFor($action, $arguments, $controllerName, $extensionName, $pluginName);

        return $uri;
    }

    /**
     * @param string $extensionName
     * @param string $pluginName
     * @param string $contextRecordId
     * @return array
     * @throws ConfigurationBuildingException
     */
    public static function buildTypoScriptRenderingConfiguration(string $extensionName, string $pluginName, string $contextRecordId): array
    {
        $configurationBuilder = new RecordRenderingConfigurationBuilder(new RenderingContext($GLOBALS['TSFE']));

        return $configurationBuilder->configurationFor($extensionName, $pluginName, $contextRecordId);
    }


}
