<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Utility;

use MEDIAESSENZ\Fullcalendar\Constants;
use MEDIAESSENZ\Fullcalendar\Domain\Model\IcalFeed;
use MEDIAESSENZ\Fullcalendar\Domain\Model\IcalFeedEventSource;
use MEDIAESSENZ\Fullcalendar\Domain\Repository\IcalFeedRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\Exception;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;

class IcalFeedUtility
{
    /**
     * @param array $eventSources
     * @param array $settings
     * @throws InvalidQueryException
     * @throws Exception
     */
    public static function addEventSources(&$eventSources, $settings)
    {
        /* Add google calendar feeds */
        $icalFeedRecordUids = GeneralUtility::intExplode(',', $settings['icalFeedRecords'], true);

        if (count($icalFeedRecordUids) > 0) {
            /** @var IcalFeedRepository $icalFeedRepository */
            $icalFeedRepository = GeneralUtility::makeInstance(IcalFeedRepository::class);
            $icalFeeds = $icalFeedRepository->findByUids((array)$icalFeedRecordUids);
            $icalFeeds = $icalFeeds->toArray();
            if (count($icalFeeds) > 0) {
                /** @var IcalFeed $icalFeed */
                foreach ($icalFeeds as $icalFeed) {
                    /** @var IcalFeedEventSource $icalFeedEventSource */
                    $icalFeedEventSource = GeneralUtility::makeInstance(IcalFeedEventSource::class);
                    $categories = $icalFeed->getCategories();
                    $firstCategory = $categories->current();
                    $classNames = [];
                    if ($categories->count() > 0) {
                        foreach ($categories as $category) {
                            $classNames[] = Constants::CATEGORY_CSS_CLASS_PREFIX . $category->getUid();
                        }
                    } else {
                        $classNames[] = Constants::CATEGORY_CSS_CLASS_PREFIX . '0';
                    }
                    // todo find a better way to filter sources
                    if (empty($settings['filter']) || ($firstCategory &&
                            in_array($firstCategory->getUid(), $settings['availableCategories']))) {
                        $color = $icalFeed->getColor() !== '' ? $icalFeed->getColor() : $firstCategory->getColor();
                        $eventSources[] = $icalFeedEventSource
                            ->setUrl($icalFeed->getUrl())
                            ->setFormat($icalFeed->getFormat())
                            ->setColor($color)
                            ->setClassName(implode(' ', $classNames))
                            ->setId((string)$icalFeed);
                    }
                }
            }
        }
    }
}
