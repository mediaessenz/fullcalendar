<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Utility;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SettingsUtility
{
    /**
     * @param array $mergedSettingsFromSetupAndFlexForms
     * @param array $configurationFromConstants
     * @param array $tsSetupSettings
     * @return array
     */
    public static function mergeWithSettingsFromConstants(array $mergedSettingsFromSetupAndFlexForms, array $configurationFromConstants, array $tsSetupSettings): array
    {
        $defaultSettingsFromConstants = $configurationFromConstants;
        $defaultSettingsFromConstants = ArrayUtility::convertBooleanStringsToBooleanRecursive($defaultSettingsFromConstants);
        $mergedSettings = $defaultSettingsFromConstants;

        ArrayUtility::mergeRecursiveWithOverrule($mergedSettings, $mergedSettingsFromSetupAndFlexForms);

        // start override
        if (isset($tsSetupSettings['overrideFlexformSettingsIfEmpty'])) {
            $overrideIfEmpty = GeneralUtility::trimExplode(',',
                $tsSetupSettings['overrideFlexformSettingsIfEmpty'], true);

            foreach ($overrideIfEmpty as $path) {
                if (ArrayUtility::isValidPath($mergedSettings, $path, '.')) {
                    $value = ArrayUtility::getValueByPath($mergedSettings, $path, '.');
                    if (empty($value)) {
                        $defaultValue = ArrayUtility::getValueByPath($defaultSettingsFromConstants, $path, '.');
                        $mergedSettings = ArrayUtility::setValueByPath($mergedSettings, $path, $defaultValue, '.');
                    } else {
                        $mergedSettings = ArrayUtility::setValueByPath($mergedSettings, $path, self::formatHeader($value), '.');
                    }
                }
            }
        }

        if (isset($tsSetupSettings['convertSettingsToInteger'])) {
            $mergedSettings = self::convertSettingsToType(
                $mergedSettings,
                GeneralUtility::trimExplode(',', $tsSetupSettings['convertSettingsToInteger'], true),
                'integer'
            );
        }

        if (isset($tsSetupSettings['convertSettingsToFloat'])) {
            $mergedSettings = self::convertSettingsToType(
                $mergedSettings,
                GeneralUtility::trimExplode(',', $tsSetupSettings['convertSettingsToFloat'], true),
                'float'
            );
        }

        if (isset($tsSetupSettings['convertSettingsToBoolean'])) {
            $mergedSettings = self::convertSettingsToType(
                $mergedSettings,
                GeneralUtility::trimExplode(',', $tsSetupSettings['convertSettingsToBoolean'], true),
                'boolean'
            );
        }

        return self::fixZeroTypoScriptNodes($mergedSettings);
    }

    /**
     * @param array $settings
     * @param array $paths
     * @param string $type
     * @return array
     */
    public static function convertSettingsToType(array $settings, array $paths, string $type): array
    {
        foreach ($paths as $path) {
            $value = ArrayUtility::getValueByPath($settings, $path, '.');
            settype($value, $type);
            $settings = ArrayUtility::setValueByPath($settings, $path, $value, '.');
        }

        return $settings;
    }

    /**
     * @param array $settings
     * @return mixed
     */
    public static function fixZeroTypoScriptNodes(array $settings): mixed
    {
        foreach ($settings as $key => $setting) {
            if (is_array($setting) && isset($setting['_typoScriptNodeValue']) && ($setting['_typoScriptNodeValue'] === '0' || $setting['_typoScriptNodeValue'] === false)) {
                unset($settings[$key]);
                $settings[$key] = false;
            }
        }

        return $settings;
    }

    /**
     * @param string $prefix
     * @return array
     */
    public static function getConfigurationFromConstants(string $prefix = 'plugin.fullcalendar.settings.'): array
    {
        $variables = [];
        if (!isset($GLOBALS['TSFE']->tmpl->flatSetup)
            || !is_array($GLOBALS['TSFE']->tmpl->flatSetup)
            || count($GLOBALS['TSFE']->tmpl->flatSetup) === 0) {
            $GLOBALS['TSFE']->tmpl->generateConfig();
        }
        $flatSetup = $GLOBALS['TSFE']->tmpl->flatSetup;
        foreach ($flatSetup as $constant => $value) {
            if (strpos($constant, $prefix) === 0) {
                $variables[substr($constant, strlen($prefix))] = $value;
            }
        }

        return self::convertDotToArray($variables);
    }

    public static function getConstants(ServerRequestInterface $request): array
    {
        return $request->getAttribute('frontend.typoscript')->getFlatSettings();
    }

    public static function getConstantsByPrefix(ServerRequestInterface $request, string $prefix, bool $stripPrefix = true): array
    {
        $constants = array_filter(
            self::getConstants($request),
            function (string $name) use ($prefix) {
                return strpos($name, $prefix . '.') === 0;
            },
            ARRAY_FILTER_USE_KEY
        );

        if ($stripPrefix === false) {
            return $constants;
        }

        $processedConstants = [];
        foreach ($constants as $name => $value) {
            $processedConstants[substr($name, strlen($prefix . '.'))] = $value;
        }

        return $processedConstants;
    }

    /**
     * Replace array elements and set _typoScriptNodeValue = 1 if already exists
     * TODO Make Recursive
     *
     * Example:
     * - array:
     * [
     *   'foo' => '1'
     * ];
     * - path: foo.bar
     * - return:
     * [
     *   'foo' => [
     *     '_typoScriptNodeValue' => '1',
     *   ],
     * ];
     *
     * @param array $array
     * @param string $path
     * @return array
     */
    public static function replaceArray(array $array, string $path): array
    {
        $pathParts = GeneralUtility::trimExplode('.', $path, true);
        $numberOfPathParts = count($pathParts);
        if ($numberOfPathParts === 1) {
            return $array;
        }

        foreach ($pathParts as $key => $pathPart) {
            if (isset($array[$pathPart]) && !is_array($array[$pathPart])) {
                $nodeValue = $array[$pathPart];
                unset($array[$pathPart]);
                if ($nodeValue !== null) {
                    $array[$pathPart] = ['_typoScriptNodeValue' => $nodeValue];
                }
            }
            if ($numberOfPathParts - 2 === $key) {
                break;
            }

        }

        return $array;
    }

    /**
     * @param array $array
     * @return array
     */
    public static function convertDotToArray(array $array): array
    {
        $newArray = [];
        foreach ($array as $key => $value) {
            $newArray = self::replaceArray($newArray, $key);
            $newArray = ArrayUtility::setValueByPath($newArray, $key, $value, '.');
        }

        return $newArray;
    }

    /**
     * @param string $code
     * @return string
     */
    public static function formatHeader(string $code): string
    {
        $code = str_replace('[SPACE1]', ' ', $code);
        $code = str_replace('[SPACE2]', ' ', $code);
        $code = str_replace(', ,', ' ', $code);
        $code = str_replace(' ,', ' ', $code);
        $code = str_replace(', ', ' ', $code);

        return trim($code);
    }

}
