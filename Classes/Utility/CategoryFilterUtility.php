<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Utility;

use MEDIAESSENZ\Fullcalendar\Constants;
use MEDIAESSENZ\Fullcalendar\Domain\Model\Category;
use MEDIAESSENZ\Fullcalendar\Domain\Model\CategoryFilter;
use MEDIAESSENZ\Fullcalendar\Domain\Repository\CategoryFilterRepository;
use MEDIAESSENZ\Fullcalendar\Domain\Repository\CategoryRepository;
use stdClass;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\Exception;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;

class CategoryFilterUtility
{
    /**
     * @param array $categoryFilters
     *
     * @return null|array
     * @throws AspectNotFoundException
     */
    public static function getHierarchicalCategoryFilterObject(array $categoryFilters): ?array
    {
        if (count($categoryFilters) === 0) {
            return null;
        }

        $categoryFiltersResult = [];
        $categoryFilterRepository = GeneralUtility::makeInstance(CategoryFilterRepository::class);

        foreach ($categoryFilters as $categoryFilterUid) {
            /** @var CategoryFilter $categoryFilter */
            $categoryFilter = $categoryFilterRepository->findByUid($categoryFilterUid);
            $categoryFilterObject = new stdClass();
            $categoryFilterObject->id = $categoryFilter->getUid();
            $categoryFilterObject->css = Constants::CATEGORY_FILTER_GROUP_CSS_CLASS_PREFIX . $categoryFilter->getUid();
            $categoryFilterObject->title = $categoryFilter->getTitle();
            $categoryFilterObject->nodes = [];
            $categories = $categoryFilter->getCategories();
            /** @var Category $category */
            foreach ($categories as $category) {
                if ($category instanceof Category) {
                    $restricedToFeGroupUids = GeneralUtility::intExplode(',', ($category->getFullcalendarFeGroupRestriction() ?? ''), true);
                    if ($restricedToFeGroupUids) {
                        /** @var FrontendUserAuthentication $frontendUserAuthentication */
                        /** @var Context $context */
                        $context = GeneralUtility::makeInstance(Context::class);
                        $userIsLoggedIn = $context->getPropertyFromAspect('frontend.user', 'isLoggedIn');
                        if (!$userIsLoggedIn) {
                            continue;
                        }
                        $userGroups = $context->getPropertyFromAspect('frontend.user', 'groupIds');
                        // Check if user has at least one of the restrictedTo usergroups
                        $result = array_intersect($restricedToFeGroupUids, $userGroups);
                        if (count($result) === 0) {
                            continue;
                        }
                    }
                    $categoryObject = new stdClass();
                    $categoryObject->text = $category->getTitle();
                    $categoryObject->id = $category->getUid();
                    $categoryObject->color = $category->getColor();
                    $categoryObject->value = Constants::CATEGORY_CSS_CLASS_PREFIX . $category->getUid();
                    $categoryObject->selected = true;
                    $categoryFilterObject->nodes[] = $categoryObject;
                }
            }

            // Sort categories by title
            usort($categoryFilterObject->nodes, function($a, $b) {
                return strcmp($a->text, $b->text);
            });

            $categoryFiltersResult[] = $categoryFilterObject;
        }

        return $categoryFiltersResult;
    }

    /**
     * @param int $categoryRoot
     * @param array $categoryFilterExcludes
     * @return null|stdClass
     * @throws Exception
     */
    public static function getHierarchicalCategoryObject(int $categoryRoot, array $categoryFilterExcludes): ?stdClass
    {
        if (in_array($categoryRoot, $categoryFilterExcludes)) {
            return null;
        }

        $categoryFilterObject = new stdClass();
        $categoryRepository =  GeneralUtility::makeInstance(CategoryRepository::class);
        /** @var Category $category */
        $category =  $categoryRoot === 0 ? 0 : $categoryRepository->findByUid($categoryRoot);

        if ($category instanceof Category) {
            $categoryFilterObject->id = $category->getUid();
            $categoryFilterObject->text = $category->getTitle();
            $categoryFilterObject->color = $category->getColor();
            $categoryFilterObject->value = Constants::CATEGORY_CSS_CLASS_PREFIX . $categoryRoot;
            $categoryFilterObject->selected = true;
        }

        $childCategories = $categoryRepository->findByParent($category)->toArray();
        if (count($childCategories) > 0) {
            $nodes = [];
            /** @var Category $childCategory */
            foreach ($childCategories as $childCategory) {
                $node = self::getHierarchicalCategoryObject($childCategory->getUid(), $categoryFilterExcludes);
                if ($node !== null) {
                    $nodes[] = $node;
                }
            }
            usort($nodes, function ($a, $b) {
                return strcmp($a->text, $b->text);
            });
            $categoryFilterObject->nodes = $nodes;
        }

        return $categoryFilterObject;
    }

    /**
     * @param object|array $categoryFilterObject
     * @return array
     */
    public static function getAvailableCategories(object|array $categoryFilterObject): array
    {
        static $uids = [];
        if (isset($categoryFilterObject->id) && $categoryFilterObject->id > 0) {
            $uids[] = $categoryFilterObject->id;
        }
        if (isset($categoryFilterObject->nodes) && is_array($categoryFilterObject->nodes)) {
            foreach ($categoryFilterObject->nodes as $node) {
                self::getAvailableCategories($node);
            }
        }
        return $uids;
    }
}
