<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Utility;

use MEDIAESSENZ\Fullcalendar\Constants;
use MEDIAESSENZ\Fullcalendar\Domain\Model\GoogleCalendarFeed;
use MEDIAESSENZ\Fullcalendar\Domain\Model\GoogleCalendarFeedEventSource;
use MEDIAESSENZ\Fullcalendar\Domain\Repository\GoogleCalendarFeedRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\Exception;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;

class GoogleCalendarUtility
{
    /**
     * @param array $eventSources
     * @param array $settings
     * @throws InvalidQueryException
     * @throws Exception
     */
    public static function addEventSources(array &$eventSources, array $settings): void
    {
        /* Add google calendar feeds */
        $googleCalendarFeedRecordUids = GeneralUtility::intExplode(',', $settings['googleCalendarFeedRecords'], true);

        if (count($googleCalendarFeedRecordUids) > 0) {
            /** @var GoogleCalendarFeedRepository $googleCalendarFeedRepository */
            $googleCalendarFeedRepository = GeneralUtility::makeInstance(GoogleCalendarFeedRepository::class);
            $googleCalendarFeeds = $googleCalendarFeedRepository->findByUids($googleCalendarFeedRecordUids);
            $googleCalendarFeeds = $googleCalendarFeeds->toArray();
            if (count($googleCalendarFeeds) > 0) {
                /** @var GoogleCalendarFeed $googleCalendarFeed */
                foreach ($googleCalendarFeeds as $googleCalendarFeed) {
                    /** @var GoogleCalendarFeedEventSource $googleCalendarFeedEventSource */
                    $googleCalendarFeedEventSource = GeneralUtility::makeInstance(GoogleCalendarFeedEventSource::class);
                    $categories = $googleCalendarFeed->getCategories();
                    $firstCategory = $categories->current();
                    $classNames = [];
                    if ($categories->count() > 0) {
                        foreach ($categories as $category) {
                            $classNames[] = Constants::CATEGORY_CSS_CLASS_PREFIX . $category->getUid();
                        }
                    } else {
                        $classNames[] = Constants::CATEGORY_CSS_CLASS_PREFIX . '0';
                    }
                    // todo find a better way to filter sources
                    if (empty($settings['filter']) || ($firstCategory &&
                            in_array($firstCategory->getUid(), $settings['availableCategories']))) {
                        $color = $googleCalendarFeed->getColor() !== '' ? $googleCalendarFeed->getColor() : $firstCategory->getColor();
                        $eventSources[] = $googleCalendarFeedEventSource
                            ->setGoogleCalendarApiKey($googleCalendarFeed->getGoogleCalendarApiKey())
                            ->setGoogleCalendarId($googleCalendarFeed->getGoogleCalendarId())
                            ->setColor($color)
                            ->setClassName(implode(' ', $classNames))
                            ->setId((string)$googleCalendarFeed);
                    }
                }
            }
        }
    }
}
