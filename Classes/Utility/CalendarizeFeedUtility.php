<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Utility;

use DateTime;
use DateTimeInterface;
use Exception;
use HDNET\Calendarize\Domain\Model\Event;
use HDNET\Calendarize\Domain\Model\Index;
use HDNET\Calendarize\Utility\HelperUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use HDNET\Calendarize\Domain\Repository\IndexRepository;

class CalendarizeFeedUtility
{
  /**
   * @param string $start
   * @param string $end
   *
   * @return string
   * @throws \HDNET\Calendarize\Exception
   * @throws Exception
   */
  public static function main($start, $end)
  {
    $start = GeneralUtility::_GP('start');
    $end = GeneralUtility::_GP('end');

    $startDateTime = new DateTime($start);
    $startTimeStamp = $startDateTime->getTimestamp();
    $endDateTime = new DateTime($end);
    $endTimeStamp = $endDateTime->getTimestamp();
    /** @var IndexRepository $indexRepository */
    $indexRepository = GeneralUtility::makeInstance(IndexRepository::class);
    $indexRepository->setIndexTypes(GeneralUtility::trimExplode(',', 'Event', true));
    $indexRepository->setOverridePageIds([0, 74]);
    $indices = $indexRepository->findByTimeSlot(
      $startTimeStamp,
      $endTimeStamp
    );
    $events = [];
    if ($indices->count() > 0) {
      foreach ($indices as $index) {
        /** @var Index $index */
        /** @var Event $originalObject */
        $originalObject = $index->getOriginalObject();
        $events[] = '{"title": "' . $originalObject->getTitle() . '", "start": "' . $index->getStartDateComplete()->format(DateTimeInterface::ATOM) . '", "end": "' . $index->getEndDateComplete()->format(DateTimeInterface::ATOM) . '"}';
      }
    }

    header('cache-control: private, max-age=0, must-revalidate, no-transform');
    header('content-type: text/javascript; charset=UTF-8');
    echo '[' . implode(',', $events) . ']';
    exit();
  }
}
