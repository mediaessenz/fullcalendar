<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Middleware;

use DateTimeImmutable;
use DateTimeZone;
use Exception;
use JsonException;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Charset\CharsetConverter;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class CalendarIcsMiddleware implements MiddlewareInterface
{
    public function __construct(private ResponseFactoryInterface $responseFactory)
    {}

    /**
     * @throws JsonException
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (!$this->isMatchingRequest($request)) {
            return $handler->handle($request);
        }

        $parameters = $request->getQueryParams();

        $requiredParameters = ['uid', 'dtstart', 'dtend'];
        if (count(array_intersect_key(array_flip($requiredParameters), $parameters)) !== count($requiredParameters)) {
            return $this->responseFactory->createResponse()->withStatus(500);
        }

        $dtStart = $parameters['dtstart'];
        $dtEnd = $parameters['dtend'];
        $allDay = (bool)($parameters['allDay'] ?? false);

        $timeZone = $parameters['timeZone'] ?: date_default_timezone_get();
        $timeZoneOffset = (int)$parameters['timeZoneOffset'] / 60;
        $timeZoneOffsetString = '+0000';
        if ($timeZoneOffset > 0) {
            $timeZoneOffsetString = sprintf('+%02d00', $timeZoneOffset);
        } else if ($timeZoneOffset < 0) {
            $timeZoneOffsetString = sprintf('%03d00', $timeZoneOffset);
        }

        $dtStartDateTime = new DateTimeImmutable($dtStart);

        $fileName =  $parameters['summary'] . '_' . $dtStartDateTime->format('Ymd');
        $fileName .= $allDay ? '' : '_' . $dtStartDateTime->format('Hi');

        $dtEndDateTime = new DateTimeImmutable($dtEnd);
        $filenameEndTime = $allDay ? '' : '_' . $dtEndDateTime->format('Hi');

        if ($allDay && $dtEndDateTime->diff($dtStartDateTime)->days > 1) {
            $filenameEndTime .= (clone($dtEndDateTime))->modify('-1 day')->format('Ymd') . $filenameEndTime;
        }

        if ($filenameEndTime) {
            $fileName .= '-' . $filenameEndTime;
        }

        $fileName = str_replace(['-_', ' '], '-', $fileName) . '.ics';

        $fileName = GeneralUtility::makeInstance(CharsetConverter::class)->utf8_char_mapping($fileName);
        // Replace unwanted characters by underscores
        $cleanFileName = rtrim(preg_replace('/[\\x00-\\x2C\\/\\x3A-\\x3F\\x5B-\\x60\\x7B-\\xBF\\xC0-\\xFF]/', '_', trim($fileName)) ?? '', '.');

        if (!$allDay && $timeZoneOffsetString !== '+0000') {
            $timeZone = 'UTC';
            $timeZoneOffset = new DateTimeZone($timeZoneOffsetString);
            $dtStartDateTime = $dtStartDateTime->setTimezone($timeZoneOffset);
            $dtEndDateTime = $dtEndDateTime->setTimezone($timeZoneOffset);
        }

        $valueType = $allDay ? ';VALUE=DATE' : '';
        $dateTimeFormat = $allDay ? 'Ymd': 'Ymd\THis' . ($timeZone === 'UTC' ? '\Z' : '');

        $content = 'BEGIN:VCALENDAR
VERSION:2.0
PRODID:MEDIAESSENZ//Fullcalendar
CALSCALE:GREGORIAN
METHOD:PUBLISH
BEGIN:VEVENT
UID:' . self::secureString($parameters['uid']) . '
SUMMARY:' . self::secureString($parameters['summary']) . '
DTSTAMP:' . date('Ymd\THis\Z') . '
DTSTART' . $valueType . ':' . $dtStartDateTime->format($dateTimeFormat) . '
DTEND' . $valueType . ':' . $dtEndDateTime->format($dateTimeFormat) . '
' . ($parameters['description'] !== 'undefined' ? 'DESCRIPTION:' . self::secureString($parameters['description']) : '') . '
' . ($parameters['location'] !== 'undefined' ? 'LOCATION:' . self::secureString($parameters['location']) : '') . '
END:VEVENT
END:VCALENDAR';

        $response = $this->responseFactory->createResponse()->withHeader('Content-Type', 'text/calendar; charset=utf-8');
        $response = $response->withAddedHeader('Content-Disposition', 'attachment; filename=' . urlencode($cleanFileName));
        $response = $response->withAddedHeader('Pragma', 'no-cache');
        $response = $response->withAddedHeader('Expire', '0');
        $response->getBody()->write($content);

        return $response;
    }

    protected function isMatchingRequest(ServerRequestInterface $request): bool
    {
        return $request->getUri()->getPath() === '/event.ics';
    }

    private static function secureString($string): string
    {
        return trim(str_replace(['\\', "\r", "\n", ',', ';'], ['\\\\', '', '\n', '\,', '\;'], html_entity_decode(strip_tags(str_replace('<br>', '\\n', $string)), ENT_COMPAT, 'UTF-8')));
    }

}
