<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar;

use MEDIAESSENZ\Fullcalendar\Controller\CalendarController;
use MEDIAESSENZ\Fullcalendar\Controller\CalendarFeedController;
use MEDIAESSENZ\Fullcalendar\Utility\CalendarizeFeedUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

class Configuration {
    /**
     * Configure Plugins
     */
    public static function configurePlugins(): void
    {
        ExtensionUtility::configurePlugin(
            'fullcalendar',
            'CalendarV3',
            [
                CalendarController::class => 'app',
            ]
        );

        ExtensionUtility::configurePlugin(
            'fullcalendar',
            'CalendarV5',
            [
                CalendarController::class => 'app',
            ]
        );

        ExtensionUtility::configurePlugin(
            'fullcalendar',
            'Feed',
            [
                CalendarFeedController::class => 'feed',
            ],
            [
                CalendarFeedController::class => 'feed',
            ]
        );
    }

    /**
     * Register Eid Scripts
     */
    public static function registerEidScripts(): void
    {
        $GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['CalendarizeFeed'] = CalendarizeFeedUtility::class . '::main';
    }
}
