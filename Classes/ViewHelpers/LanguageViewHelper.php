<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\ViewHelpers;

use Closure;
use stdClass;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class LanguageViewHelper extends AbstractViewHelper
{

    use CompileWithRenderStatic;

    protected $escapeOutput = false;

    /**
     * Initialize the arguments.
     */
    public function initializeArguments(): void
    {
        $this->registerArgument('keys', 'string', 'Comma separated list of language keys', false, '');
        $this->registerArgument('extensionName', 'string', 'extension name', false, 'fullcalendar');
    }

    /**
     * get country infos from a given ISO3
     *
     * @param array $arguments
     * @param Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     *
     * @return mixed
     */
    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $keys = GeneralUtility::trimExplode(',', $arguments['keys'], true);
        $result = new stdClass();
        foreach ($keys as $key) {
            $propertyName = str_replace('_', '', preg_replace_callback('/_(.?)/', function ($e) {
                return strtoupper($e[0]);
            }, $key));
            $result->$propertyName = LocalizationUtility::translate($key, $arguments['extensionName']);
        }
        return json_encode($result);
    }

}
