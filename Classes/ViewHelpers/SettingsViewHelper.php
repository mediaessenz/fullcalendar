<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\ViewHelpers;

use Closure;
use MEDIAESSENZ\Fullcalendar\Domain\Model\AbstractEventSource;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class SettingsViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    protected $escapeOutput = false;

    /**
     * Initialize the arguments.
     */
    public function initializeArguments(): void
    {
        $this->registerArgument('settings', 'array', 'Settings array', true, []);
        $this->registerArgument('json', 'boolean', 'Return json', false, true);
    }

    /**
     * get country infos from a given ISO3
     *
     * @param array $arguments
     * @param Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     *
     * @return mixed
     */
    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $settings = (array)$arguments['settings'];
        $json = (bool)$arguments['json'];

        $result = self::cleanup($settings);
        if ($json) {
            return json_encode($result);
        }
        return $result;
    }

    /**
     * @param array $settings
     * @return array
     */
    public static function cleanup($settings): array
    {
        $result = [];
        foreach ($settings as $key => $value) {
            if (is_array($value)) {
                if (isset($value['_typoScriptNodeValue'])) {
                    if ($value['_typoScriptNodeValue'] !== false) {
                        unset($value['_typoScriptNodeValue']);
                        $result[$key] = self::cleanup($value);
                    }
                } else {
                    $result[$key] = self::cleanup($value);
                }
            } elseif ($value instanceof AbstractEventSource) {
                $properties = $value->_getProperties();
                $properties = self::removeEmptyProperties($properties);
                $object = (object)$properties;
                $result[$key] = $object;
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * @param array $properties
     * @return array
     */
    public static function removeEmptyProperties($properties): array
    {
        return array_filter($properties, function ($property, $propertyName) {
            return !(!is_bool($property) && ((is_string($property) && trim($property) === '') || $property === null || (is_array($property) && count($property) === 0) || ($propertyName === 'id' && $property === 0)));
        }, ARRAY_FILTER_USE_BOTH);
    }

}
