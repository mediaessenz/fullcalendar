<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar;

class Constants
{
    const CATEGORY_CSS_CLASS_PREFIX = 'fc-category-';
    const CATEGORY_FILTER_GROUP_CSS_CLASS_PREFIX = 'fc-category-and-filter-';
}
