<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Controller;

use Helhum\TyposcriptRendering\Configuration\ConfigurationBuildingException;
use MEDIAESSENZ\Fullcalendar\Utility\CalendarizeUtility;
use MEDIAESSENZ\Fullcalendar\Utility\CategoryFilterUtility;
use MEDIAESSENZ\Fullcalendar\Utility\GoogleCalendarUtility;
use MEDIAESSENZ\Fullcalendar\Utility\IcalFeedUtility;
use MEDIAESSENZ\Fullcalendar\Utility\SettingsUtility;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Object\Exception;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Alexander Grein <alexander.grein@gmail.com>, MEDIA::ESSENZ
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package fullcalendar
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class CalendarController extends ActionController
{

    /**
     * @param TypoScriptService $typoScriptService
     */
    public function __construct(protected TypoScriptService $typoScriptService) {
    }

  /**
   * initialize action
   * @return void
   * @throws InvalidQueryException
   * @throws ConfigurationBuildingException
   * @throws AspectNotFoundException
   * @throws Exception
   */
    public function initializeAction(): void
    {
        parent::initializeAction();

        $fullTyposcriptSettings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $tsSetupSettings = $this->typoScriptService->convertTypoScriptArrayToPlainArray($fullTyposcriptSettings['plugin.']['fullcalendar.']['settings.']);

        $versionInformation = GeneralUtility::makeInstance(Typo3Version::class);
        if ($versionInformation->getMajorVersion() <= 12) {
            $defaultSettings = SettingsUtility::getConfigurationFromConstants('plugin.fullcalendar.settings.defaults.');
        } else {
            $defaultSettings = SettingsUtility::convertDotToArray(SettingsUtility::getConstantsByPrefix($this->request, 'plugin.fullcalendar.settings.defaults'));
        }

        $this->settings = SettingsUtility::mergeWithSettingsFromConstants(
            $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS),
            $defaultSettings,
            $tsSetupSettings
        );

        $this->settings['categories'] = CategoryFilterUtility::getHierarchicalCategoryObject((int)$this->settings['categoryFilterRoot'], GeneralUtility::intExplode(',', $this->settings['categoryFilterExcludes'] ?? '', true));
        $this->settings['filter'] = CategoryFilterUtility::getHierarchicalCategoryFilterObject(GeneralUtility::intExplode(',', $this->settings['categoryFilters'] ?? '', true));
        $this->settings['availableCategories'] = CategoryFilterUtility::getAvailableCategories($this->settings['categories'] ?? []);
        $this->settings['availableFilters'] = CategoryFilterUtility::getAvailableCategories($this->settings['filter'] ?? []);

        // Add event sources
        $eventSources = [];

        // Add google calendar feed events
        GoogleCalendarUtility::addEventSources($eventSources, $this->settings);

        // Add ical calendar feed events
        IcalFeedUtility::addEventSources($eventSources, $this->settings);

        if ((new Typo3Version())->getMajorVersion() > 11) {
            $currentContentObject = $this->request->getAttribute('currentContentObject');
        } else {
            $currentContentObject = $this->configurationManager->getContentObject();
        }
        // Add calendarize calendar feed events
        CalendarizeUtility::addEventSources($eventSources, $this->settings, $currentContentObject, $this->uriBuilder);

        $this->settings['eventSources'] = $eventSources;
    }

    /**
     * App action
     */
    public function appAction()
    {
        if ((new Typo3Version())->getMajorVersion() > 11) {
            $currentContentObject = $this->request->getAttribute('currentContentObject');
        } else {
            $currentContentObject = $this->configurationManager->getContentObject();
        }
        $this->view->assign('id', 'fc' . $currentContentObject->data['uid']);
        return $this->htmlResponse();
    }
}
