<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Fullcalendar\Controller;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use HDNET\Calendarize\Domain\Model\Index;
use HDNET\Calendarize\Domain\Repository\IndexRepository;
use HDNET\Calendarize\Domain\Model\Event as CalendarizeEvent;
use MEDIAESSENZ\Fullcalendar\Constants;
use MEDIAESSENZ\Fullcalendar\Domain\Model\Category;
use MEDIAESSENZ\Fullcalendar\Domain\Model\Event;
use MEDIAESSENZ\Fullcalendar\Domain\Repository\CategoryRepository;
use MEDIAESSENZ\Fullcalendar\Utility\CalendarizeUtility;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\JsonView;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extensionmanager\Domain\Repository\ExtensionRepository;
use TYPO3\CMS\Extensionmanager\Service\ExtensionManagementService;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

class CalendarFeedController extends ActionController
{

    public function __construct(
        private readonly IndexRepository $indexRepository,
        private readonly CategoryRepository $categoryRepository,
    ) {
        $this->defaultViewObjectName = JsonView::class;
    }

    /**
     * @throws Exception
     */
    public function feedAction(): ResponseInterface
    {
        $parsedBody = $this->request->getParsedBody();
        $timezone = $parsedBody['timeZone'] ?? false;
        $startDateTime = new DateTimeImmutable($parsedBody['start'] ?? 'now');
        $endDateTime = isset($parsedBody['end']) ? new DateTimeImmutable($parsedBody['end']) : $startDateTime->modify('+1 year -1 second');
        $timezone = $timezone ?? $startDateTime->getTimezone();
        $this->indexRepository->setIndexTypes(GeneralUtility::trimExplode(',', 'Event', true));
        $calendarizeStoragePids = GeneralUtility::intExplode(',', ($this->settings['calendarizeStoragePids'] ?? ''), true);
        $calendarizeStorageRecursiveDepth = (int)($this->settings['calendarizeStorageRecursiveDepth'] ?? 0);
        if (count($calendarizeStoragePids) > 0 && $calendarizeStorageRecursiveDepth > 0) {
            // Add all pages to defined depth
            foreach ($calendarizeStoragePids as $calendarizeStoragePid) {
                $calendarizeStoragePids = array_merge($calendarizeStoragePids, CalendarizeUtility::getTreePids($calendarizeStoragePid, $calendarizeStorageRecursiveDepth));
            }
            $calendarizeStoragePids = array_keys(array_flip($calendarizeStoragePids));
        }
        $this->indexRepository->setOverridePageIds($calendarizeStoragePids);
        $indices = $this->indexRepository->findByTimeSlot($startDateTime, $endDateTime);
        $events = new ObjectStorage();

        if ($indices->count() > 0) {
            /** @var ContentObjectRenderer $contentRenderer */
            $contentRenderer = GeneralUtility::makeInstance(ContentObjectRenderer::class);
            $context = GeneralUtility::makeInstance(Context::class);
            $userIsLoggedIn = $context->getPropertyFromAspect('frontend.user', 'isLoggedIn');
            $frontendUserGroups = $context->getPropertyFromAspect('frontend.user', 'groupIds');
            $categoriesPreFilter = [];

            if ($this->settings['categoryPreFilter'] ?? false) {
                $categoriesPreFilter = GeneralUtility::intExplode(',', $this->settings['categoryPreFilter'], true);
            }
            $eventNeedsMatchingCategories = count($categoriesPreFilter);

            foreach ($indices as $index) {
                /** @var Index $index */
                /** @var CalendarizeEvent $originalObject */
                $originalObject = $index->getOriginalObject();
                $originalObjectCategories = $originalObject->getCategories();
                $originalObjectHasCategories = $originalObjectCategories instanceof ObjectStorage && $originalObjectCategories->count() > 0;

                // categories are sys categories -> convert sys categories to fullcalendar categories
                $newOriginalObjectCategories = new ObjectStorage();
                foreach ($originalObjectCategories as $category) {
                    $newOriginalObjectCategories->attach($this->categoryRepository->findByUid($category->getUid()));
                }
                $originalObjectCategories = $newOriginalObjectCategories;
                $firstCategory = null;

                // user group filter
                if ($originalObjectHasCategories) {
                    $firstCategory = $originalObjectCategories->current();
                    $addDate = false;
                    /** @var Category  $category */
                    foreach ($originalObjectCategories as $category) {
                        $fullcalendarFeGroupRestriction = GeneralUtility::intExplode(',', ($category->getFullcalendarFeGroupRestriction() ?? ''), true);
                        if (!$fullcalendarFeGroupRestriction || array_intersect($frontendUserGroups, $fullcalendarFeGroupRestriction)) {
                            // user has needed user group of date -> skip further user group checks
                            $addDate = true;
                            break;
                        }
                    }
                    if (!$addDate) {
                        // user has not needed user group of date -> skip current date
                        continue;
                    }
                }

                // category pre filter
                if ($eventNeedsMatchingCategories > 0) {
                    if (false === $originalObjectHasCategories) {
                        continue;
                    } else {
                        $eventHasMatchingCategories = 0;
                        foreach ($originalObjectCategories as $category) {
                            foreach ($categoriesPreFilter as $categoryPreFilter) {
                                if ($category->getUid() === $categoryPreFilter) {
                                    $eventHasMatchingCategories += 1;
                                }
                            }
                        }
                        if ($eventHasMatchingCategories !== $eventNeedsMatchingCategories) {
                            continue;
                        }
                    }
                }

                $classNames = [];
                if ($originalObjectHasCategories) {
                    foreach ($originalObjectCategories as $category) {
                        $classNames[] = Constants::CATEGORY_CSS_CLASS_PREFIX . $category->getUid();
                    }
                }

                $locationLink = '';
                if (!empty($originalObject->getOrganizerLink())) {
                    $locationLink = $contentRenderer->typoLink_URL([
                        'forceAbsoluteUrl' => true,
                        'parameter' => $originalObject->getLocationLink(),
                    ]);
                }
                $organizerLink = '';
                if (!empty($originalObject->getOrganizerLink())) {
                    $organizerLink = $contentRenderer->typoLink_URL([
                        'forceAbsoluteUrl' => true,
                        'parameter' => $originalObject->getOrganizerLink(),
                    ]);
                }

                $startDateComplete = $index->getStartDateComplete();
                $endDateComplete = $index->getEndDateComplete();
                $format = DateTimeInterface::ATOM;

                if ($index->isAllDay()) {
                    $endDateComplete->modify('+1 day');
                    $format = 'Y-m-d';
                }

                if ($timezone !== false && $timezone !== 'local' && $timezone !== '') {
                    $dateTimeZone = $timezone instanceof DateTimeZone ? $timezone : new DateTimeZone($timezone);
                    $startDateComplete->setTimezone($dateTimeZone);
                    $endDateComplete->setTimezone($dateTimeZone);
                }

                // create event object
                $event = new Event();
                $event->setId(get_class($originalObject) . ':' . $originalObject->getUid())
                    ->setTitle($originalObject->getTitle())
                    ->setStart($startDateComplete->format($format))
                    ->setEnd($endDateComplete->format($format))
                    ->setAllDay($index->isAllDay())
                    ->setLocation($originalObject->getLocation())
                    ->setLocationLink($locationLink)
                    ->setDescription($contentRenderer->parseFunc($originalObject->getDescription(), [], '< lib.parseFunc_RTE'))
                    ->setOrganizer($originalObject->getOrganizer())
                    ->setOrganizerLink($organizerLink)
                    ->setClassName(trim(implode(' ', $classNames)));

                if ($firstCategory) {
                    $event->setColor($firstCategory->getColor());
                }

                if ($this->settings['calendarizeDetailPid'] ?? false) {
                    $uri = $this->uriBuilder->reset()
                        ->setTargetPageUid($this->settings['calendarizeDetailPid'])
                        ->setTargetPageType(0)
                        ->setNoCache(false)
                        ->setSection('')
                        ->setFormat('')
                        ->setLinkAccessRestrictedPages(false)
                        ->setArguments([])
                        ->setCreateAbsoluteUri(true)
                        ->setAddQueryString(false)
                        ->setArgumentsToBeExcludedFromQueryString([])
                        ->uriFor('detail', ['index' => $index->getUid()], 'Calendar', 'calendarize', 'Calendar');
                    $event->setDetailsUrl($uri);
                }

                if ($this->settings['calendarizeBookingPid'] ?? false) {
                    $uri = $this->uriBuilder->reset()
                        ->setTargetPageUid($this->settings['calendarizeBookingPid'])
                        ->setTargetPageType(0)
                        ->setNoCache(false)
                        ->setSection('')
                        ->setFormat('')
                        ->setLinkAccessRestrictedPages(false)
                        ->setArguments([])
                        ->setCreateAbsoluteUri(true)
                        ->setAddQueryString(false)
                        ->setArgumentsToBeExcludedFromQueryString([])
                        ->uriFor('booking', ['index' => $index->getUid()], 'Booking', 'calendarize', 'Calendar');
                    $event->setBookingUrl($uri);
                }

                $events->attach($event);
            }
        }

        $this->view->assign('value', $events->toArray());
        return $this->jsonResponse();
    }

}
