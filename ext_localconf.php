<?php
defined('TYPO3') or die();

\MEDIAESSENZ\Fullcalendar\Configuration::configurePlugins();
\MEDIAESSENZ\Fullcalendar\Configuration::registerEidScripts();
