#
# Table structure for table 'sys_category'
#
CREATE TABLE `sys_category`
(
	`fullcalendar_fe_group_restriction` text                  DEFAULT NULL,
	`color`                             varchar(255) NOT NULL DEFAULT ''
);

CREATE TABLE `tx_fullcalendar_domain_model_categoryfilter`
(
	`uid`        int(10) unsigned NOT NULL AUTO_INCREMENT,
	`pid`        int(10) unsigned NOT NULL DEFAULT 0,
	`title`      text DEFAULT NULL,
	`categories` text DEFAULT NULL,
	PRIMARY KEY (`uid`) USING BTREE,
	KEY          `parent` (`pid`) USING BTREE
);

CREATE TABLE `tx_fullcalendar_domain_model_googlecalendarfeed`
(
	`uid`                     int(10) unsigned NOT NULL AUTO_INCREMENT,
	`categories`              int(11) NOT NULL DEFAULT 0,
	`pid`                     int(10) unsigned NOT NULL DEFAULT 0,
	`title`                   text                  DEFAULT NULL,
	`google_calendar_api_key` text                  DEFAULT NULL,
	`google_calendar_id`      text                  DEFAULT NULL,
	`color`                   varchar(255) NOT NULL DEFAULT '',
	`class_name`              text                  DEFAULT NULL,
	`hidden`                  smallint(5) unsigned NOT NULL DEFAULT 0,
	`starttime`               int(10) unsigned NOT NULL DEFAULT 0,
	`endtime`                 int(10) unsigned NOT NULL DEFAULT 0,
	`fe_group`                varchar(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`uid`) USING BTREE,
	KEY                       `parent` (`pid`, `hidden`)
);

CREATE TABLE `tx_fullcalendar_domain_model_icalfeed`
(
	`uid`        int(10) unsigned NOT NULL AUTO_INCREMENT,
	`categories` int(11) NOT NULL DEFAULT 0,
	`pid`        int(10) unsigned NOT NULL DEFAULT 0,
	`title`      text                  DEFAULT NULL,
	`url`        varchar(255) NOT NULL DEFAULT '',
	`color`      varchar(255) NOT NULL DEFAULT '',
	`class_name` text                  DEFAULT NULL,
	`hidden`     smallint(5) unsigned NOT NULL DEFAULT 0,
	`starttime`  int(10) unsigned NOT NULL DEFAULT 0,
	`endtime`    int(10) unsigned NOT NULL DEFAULT 0,
	`fe_group`   varchar(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`uid`) USING BTREE,
	KEY          `parent` (`pid`, `hidden`)
);
