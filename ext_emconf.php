<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "fullcalendar".
 *
 * Auto generated 18-10-2017 11:49
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['fullcalendar'] = [
    'title' => 'Fullcalendar',
    'description' => 'Includes the jQuery Plugin FullCalendar, which generates a skinable calendar with different views (month, week, day, week list, day list etc.).',
    'category' => 'plugin',
    'version' => '3.0.0',
    'state' => 'stable',
    'author' => 'Alexander Grein',
    'author_email' => 'alexander.grein@gmail.com',
    'author_company' => 'MEDIA::ESSENZ',
    'constraints' =>
        [
            'depends' =>
                [
                    'typo3' => '11.5.0-13.4.99',
                ],
            'suggests' =>
                [
                    'calendarize' => '10.2.0-',
                ],
        ],
    'autoload' =>
        [
            'psr-4' =>
                [
                    'MEDIAESSENZ\\Fullcalendar\\' => 'Classes',
                ],
        ],
];
