module.exports = {
  outputDir: '../../../Resources/Public',
  publicPath: '/typo3conf/ext/fullcalendar/Resources/Public/',
  filenameHashing: false,
  productionSourceMap: false,
  runtimeCompiler: false,

  configureWebpack: {
    externals: {
      // shows how we can rely on browser globals instead of bundling these dependencies,
      // in case we want to access jQuery from a CDN or if we want an easy way to
      // avoid loading all moment locales: https://github.com/moment/moment/issues/1435
      // jquery: 'jQuery',
      // moment: 'moment',
    },
    entry: {
      fullcalendar: './src/main.js',
    },
    output: {
      filename: process.env.VUE_CLI_MODERN_BUILD ? 'JavaScript/V5/BT5/[name].js' : 'JavaScript/V5/BT5/[name]-legacy.js',
      chunkFilename: process.env.VUE_CLI_MODERN_BUILD ? 'JavaScript/V5/BT5/[name].js' : 'JavaScript/V5/BT5/[name]-legacy.js',
    },
  },

  chainWebpack: (config) => {
    config.entryPoints.delete('app');
    config.module.rule('images').use('url-loader').loader('url-loader').tap((options) => {
      // eslint-disable-next-line no-param-reassign
      options.fallback.options.name = 'Images/V5/BT5/[name].[ext]';
      return options;
    });
    config.module.rule('svg').use('file-loader').loader('file-loader').tap((options) => {
      // eslint-disable-next-line no-param-reassign
      options.name = 'Images/V5/BT5/[name].[ext]';
      return options;
    });
    config.module.rule('media').use('url-loader').loader('url-loader').tap((options) => {
      // eslint-disable-next-line no-param-reassign
      options.fallback.options.name = 'Media/V5/BT5/[name].[ext]';
      return options;
    });
    config.module.rule('fonts').use('url-loader').loader('url-loader').tap((options) => {
      // eslint-disable-next-line no-param-reassign
      options.fallback.options.name = 'Fonts/V5/BT5/[name].[ext]';
      return options;
    });
  },

  css: {
    extract: {
      filename: 'Css/V5/BT5/[name].css',
      chunkFilename: 'Css/V5/BT5/[name].css',
    },
  },

  pluginOptions: {
    moment: {
      locales: [
        'de',
        // 'fr',
        // 'it',
        // 'es',
        // 'pt',
        // 'dk',
        // 'nl',
        // 'hu',
        // 'pl',
        // 'sk',
        // 'cs',
        // 'kr',
        // 'ru',
        // 'sv',
      ],
    },
  },
};
