import Vue from 'vue';
import {
  BModal,
  BFormCheckbox,
} from 'bootstrap-vue';
// eslint-disable-next-line
import App from './App';
import CategoryRoot from './components/CategoryRoot';
import CategoryItem from './components/CategoryItem';
import './styles/main.scss';

Vue.config.productionTip = false;

Vue.component('b-modal', BModal);
Vue.component('b-checkbox', BFormCheckbox);
Vue.component('category-root', CategoryRoot);
Vue.component('category-item', CategoryItem);

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line no-restricted-syntax
  for (const id of window.fullcalendarids) {
    const filterSettingsPropertyName = `${id}-filter-settings`;
    const disabledCategoriesPropertyName = `${id}-disabled-categories`;
    const viewPropertyName = `${id}-view`;
    new Vue({
      id,
      data: {
        settings: window[id].settings,
        lang: window[id].lang,
        filterSettingsPropertyName,
        defaultFilterSettings: window[id].settings.filter || [],
        disabledCategoriesPropertyName,
        defaultDisabledCategories: [],
        viewPropertyName,
        defaultView: window[id].settings.defaultView || 'timeGridWeek',
      },
      methods: {
        determineDisabledCategories(filter) {
          const disabledCategories = [];
          filter.forEach((element) => {
            const { nodes } = element;
            if (nodes.length > 0) {
              nodes.forEach((node) => {
                if (node.selected === false) {
                  disabledCategories.push(node.id);
                }
              });
            }
          });
          return disabledCategories;
        },
        saveDisabledCategories(filter) {
          const disabledCategories = this.determineDisabledCategories(filter);
          this[disabledCategoriesPropertyName] = disabledCategories;
          localStorage.setItem(disabledCategoriesPropertyName, JSON.stringify(disabledCategories));
        },
        disableCategoriesInFilter() {
          const disabledCategories = this[disabledCategoriesPropertyName];
          const filterSettings = this[filterSettingsPropertyName];
          filterSettings.forEach((element) => {
            const { nodes } = element;
            if (nodes.length > 0) {
              nodes.forEach((node) => {
                if (typeof disabledCategories !== 'undefined' && disabledCategories.includes(node.id)) {
                  // eslint-disable-next-line no-param-reassign
                  node.selected = false;
                }
              });
            }
          });
          this[filterSettingsPropertyName] = filterSettings;
        },
        saveView(view) {
          this[viewPropertyName] = view;
          localStorage.setItem(viewPropertyName, view);
        },
      },
      created() {
        this[filterSettingsPropertyName] = this.defaultFilterSettings;
        if (localStorage.getItem(viewPropertyName)) {
          try {
            let view = localStorage.getItem(viewPropertyName);
            if (view === 'undefined' || !['timeGridDay', 'timeGridWeek', 'dayGridWeek', 'dayGridMonth', 'listDay', 'listWeek', 'listMonth', 'listYear'].includes(view)) {
              view = this.defaultView;
            }
            this.saveView(view);
          } catch (e) {
            localStorage.removeItem(viewPropertyName);
          }
        } else {
          this.saveView(this.defaultView);
        }
        if (localStorage.getItem(disabledCategoriesPropertyName)) {
          try {
            let disabledCategories = JSON.parse(localStorage.getItem(disabledCategoriesPropertyName));
            if (disabledCategories === 'undefined') {
              disabledCategories = this.defaultDisabledCategories;
            }
            this[disabledCategoriesPropertyName] = disabledCategories;
          } catch (e) {
            localStorage.removeItem(disabledCategoriesPropertyName);
          }
        } else {
          // this.saveDisabledCategories(this.defaultDisabledCategories);
        }
        this.disableCategoriesInFilter();
      },
      render: (h) => h(App),
    }).$mount(`#${id}`);
  }
});
