﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


What does it do?
^^^^^^^^^^^^^^^^

- This extension includes the jQuery Plugin `FullCalendar
  <http://fullcalendar.io/>`_ , which generates a skinable
  Calendar with different views (month, week, day, week list, day list)
  from Google Calendar XML Feed(s).

- Version 3.x uses Extbase and Fluid and needs at least TYPO3 6.0

- The extension is localized in english and german. Contact me, if you
  would like to add your language.

- For examples of the different views visit `http://fullcalendar.io
  <http://fullcalendar.io/>`_

- me\_google\_calendar 3.2.x uses version 3.3.1 of the FullCalendar Script
  and requires jQuery 2.x and jQuery-UI 1.9.x

