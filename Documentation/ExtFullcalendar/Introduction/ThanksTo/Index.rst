﻿﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Thanks To
^^^^^^^^^

- Adam Shaw for this amazing jQuery Plugin
- Helge Sascha Anders Koziele for sponsoring Google API 3 Update
- Fred Hartmann und Tobias Döring GbR for sponsoring Google API 3 Update
- m|r|o|net - Internetsolutions for sponsoring Google API 3 Update
- Evang.-Luth. Pfarramt St. Johannes Gilching-Weßling for sponsoring Google API 3 Update

