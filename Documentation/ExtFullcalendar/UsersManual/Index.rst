﻿.. include:: Images.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Users manual
------------

- Install the extension from the TYPO3 Extension Repository.

- Go to your Page-Template and include the google calendar TS Extension
  Template.
  
  |img-4|

- Generate Google Calendar Feed Record(s) in a sysfolder of our choice
  (e.g. general storage) and fill in the fields. The CSS Class value
  will be added to the corresponding calendar events (styling example
  see below).
  Since 3.1.0 it is also possible to restrict a feed to a specific feuser group using the access tab.

  |img-5|

  |img-6|

  On Nov 17th 2014, Google shut down V1 and V2 of their Calendar APIs.
  To access your feeds again, a Google Calendar API key is now required:

  - Go to the Google Developer Console (https://console.developers.google.com/ )
    and create a new project (it might take a second).
  - Once in the project, go to APIs & auth > APIs on the sidebar.
  - Find "Calendar API" in the list and turn it ON.
  - On the sidebar, click APIs & auth > Credentials.
  - In the "Public API access" section, click "Create new Key".
  - Choose "Browser key".
  - If you know what domains will host your calendar, enter them into the box.
    Otherwise, leave it blank. You can always change it later.
  - Your new API key will appear. It might take second or two before it starts working.



  Make your Google Calendar public:

  - In the Google Calendar interface, locate the "My calendars" area on the left.
  - Hover over the calendar you need and click the downward arrow.
  - A menu will appear. Click "Share this Calendar".
  - Check "Make this calendar public".
  - Make sure "Share only my free/busy information" is unchecked.
  - Click "Save".



  Obtain your Google Calendar's ID:

  - In the Google Calendar interface, locate the "My calendars" area on the left.
  - Hover over the calendar you need and click the downward arrow.
  - A menu will appear. Click "Calendar settings".
  - In the "Calendar Address" section of the screen, you will see your Calendar ID.
    It will look something like "abcd1234@group.calendar.google.com".

  |img-7|

- Under Css you can enter one of the predefined style definitions, who comes with
  this extension:

  fc-red, fc-green, fc-blue, fc-grey, fc-orange, fc-purple

  If you want to show all events from a calendar e.g. in red, just add the predefined
  CSS Class “fc-red” to the Google Calendar Feed Record.
  Or define your own (example for “my-color”):

  .fc-event.my-color,
  .fc-agenda .fc-event. my-color .fc-event-time,
  .fc-event.my-color .fc-event-inner.fc-event-skin,
  .fc-event. my-color a {background-color:#ff0000;border-color:#ff0000;color:#fff;}

- Finally add a “General Plugin” on the page you like to view the calendar and select
  “Calendar” under tab “Plugin”.
- Configure the output as you like (you can also configure the plugin globally by using
  the constant editor)
- Have fun ;-)

- If you like my plugin, I'd appreciate a small donation: `Flattr
  <http://flattr.com/thing/185e92a2c3c8885eb296f8e37386bcd9>`_ or `PayPal
  <https://www.paypal.com/de/cgi-bin/webscr?cmd=_send-money&nav=1&email=a.grein@mediaessenz.de>`_
