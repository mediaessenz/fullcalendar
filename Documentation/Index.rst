﻿.. include:: Images.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)

====================
EXT: Google Calendar
====================

:Extensionkey:
      fullcalendar

:Keywords:
      Calendar, FullCalendar, jQuery

:Languages:
      en

:Created:
      2017-10-18

:Last Changed:
      2017-10-18

:Author:
      Alexander Grein

:Email:
      alexander.grein@gmail.com


Table of Content
^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   ExtFullcalendar/Index

