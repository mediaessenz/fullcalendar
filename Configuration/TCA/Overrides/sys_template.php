<?php
defined('TYPO3') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('fullcalendar', 'Configuration/TypoScript/V3', 'Fullcalendar V3');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('fullcalendar', 'Configuration/TypoScript/V5', 'Fullcalendar V5');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('fullcalendar', 'Configuration/TypoScript/V6', 'Fullcalendar V6');
