<?php
defined('TYPO3') or die();

$tempCols = [
    'fullcalendar_fe_group_restriction' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:fullcalendar/Resources/Private/Language/locallang_db.xlf:sys_category.fullcalendar_fe_group_restriction',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectMultipleSideBySide',
            'size' => 5,
            'maxitems' => 20,
            'items' => [
                [
                    'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hide_at_login',
                    -1,
                ],
                [
                    'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.any_login',
                    -2,
                ],
                [
                    'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.usergroups',
                    '--div--',
                ],
            ],
            'exclusiveKeys' => '-1,-2',
            'foreign_table' => 'fe_groups',
            'foreign_table_where' => 'ORDER BY fe_groups.title',
        ],
    ],
    'color' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:fullcalendar/Resources/Private/Language/locallang_db.xlf:sys_category.color',
        'description' => 'renderType=colorpicker',
        'config' => [
            'type' => 'input',
            'renderType' => 'colorpicker',
            'size' => 10,
        ],
    ],
];

// add new fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $tempCols);
unset($tempCols);

// add fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'sys_category',
    '--div--;LLL:EXT:fullcalendar/Resources/Private/Language/locallang_db.xlf:sys_category.tab.fullcalendar,color,fullcalendar_fe_group_restriction'
);
