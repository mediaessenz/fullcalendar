<?php
defined('TYPO3') or die();

// Register plugin V3
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'fullcalendar',
    'CalendarV3',
    'Fullcalendar V3',
    'EXT:fullcalendar/Resources/Public/Icons/Extension.svg'
);

// Hide not used fields
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['fullcalendar_calendarv3'] = 'code,layout,select_key,pages,recursive';

// Add flexform
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['fullcalendar_calendarv3'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('fullcalendar_calendarv3', 'FILE:EXT:fullcalendar/Configuration/FlexForms/V3/Calendar.xml');

// Register plugin V5
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'fullcalendar',
    'CalendarV5',
    'Fullcalendar V5',
    'EXT:fullcalendar/Resources/Public/Icons/Extension.svg'
);

// Hide not used fields
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['fullcalendar_calendarv5'] = 'code,layout,select_key,pages,recursive';

// Add flexform
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['fullcalendar_calendarv5'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('fullcalendar_calendarv5', 'FILE:EXT:fullcalendar/Configuration/FlexForms/V5/Calendar.xml');
