<?php

return [
    'ctrl' => [
        'title' => 'LLL:EXT:fullcalendar/Resources/Private/Language/locallang.xlf:tx_fullcalendar_domain_model_categoryfilter',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'versioningWS' => true,
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,',
        'iconfile' => 'EXT:fullcalendar/Resources/Public/Icons/CategoryFilter.svg',
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, title, categories',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden, title, categories'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'title' => [
            'label' => 'LLL:EXT:fullcalendar/Resources/Private/Language/locallang.xlf:tx_fullcalendar_domain_model_categoryfilter.title',
            'exclude' => true,
            'config' => [
                'type' => 'input',
            ],
        ],
        'categories' => [
            'label' => 'LLL:EXT:fullcalendar/Resources/Private/Language/locallang.xlf:tx_fullcalendar_domain_model_categoryfilter.categories',
            'exclude' => true,
            'config' => [
                'type' => 'select',
                'minitems' => 0,
                'maxitems' => 100,
                'foreign_table' => 'sys_category',
                'foreign_table_where' => 'AND sys_category.sys_language_uid IN (-1, 0) ORDER BY sys_category.sorting ASC',
                'renderType' => 'selectTree',
                'size' => 10,
                'treeConfig' => [
                    'appearance' => [
                        'expandAll' => 1,
                        'maxLevels' => 99,
                        'showHeader' => 1,
                    ],
                    'parentField' => 'parent',
                ],
            ],
        ],
    ],
];
