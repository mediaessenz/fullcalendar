<?php

return [
    'ctrl' => [
        'title' => 'Google Calendar Feed',
        'label' => 'title',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group',
        ],
        'iconfile' => 'EXT:fullcalendar/Resources/Public/Icons/Feed.svg',
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden,title,google_calendar_api_key,google_calendar_id,color,class_name,categories,fe_group',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden,title,google_calendar_api_key,google_calendar_id,color,class_name,categories,--div--;LLL:EXT:fullcalendar/Resources/Private/Language/locallang_db.xlf:access,starttime,endtime,fe_group'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'fe_group' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.fe_group',
        ],
        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:fullcalendar/Resources/Private/Language/locallang_db.xlf:tx_fullcalendar_domain_model_googlecalendarfeed.title',
            'config' => [
                'type' => 'input',
            ],
        ],
        'color' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:fullcalendar/Resources/Private/Language/locallang_db.xlf:tx_fullcalendar_domain_model_googlecalendarfeed.color',
            'description' => 'renderType=colorpicker',
            'config' => [
                'type' => 'input',
                'renderType' => 'colorpicker',
                'size' => 10,
            ],
        ],
        'class_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:fullcalendar/Resources/Private/Language/locallang_db.xlf:tx_fullcalendar_domain_model_googlecalendarfeed.class_name',
            'config' => [
                'type' => 'input',
            ],
        ],
        'google_calendar_api_key' => [
            'exclude' => true,
            'label' => 'LLL:EXT:fullcalendar/Resources/Private/Language/locallang_db.xlf:tx_fullcalendar_domain_model_googlecalendarfeed.google_calendar_api_key',
            'config' => [
                'type' => 'input',
            ],
        ],
        'google_calendar_id' => [
            'exclude' => true,
            'label' => 'LLL:EXT:fullcalendar/Resources/Private/Language/locallang_db.xlf:tx_fullcalendar_domain_model_googlecalendarfeed.google_calendar_id',
            'config' => [
                'type' => 'input',
            ],
        ],
        'categories' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.categories',
            'config' => [
                'type' => 'category'
            ]
        ]
    ],
];
