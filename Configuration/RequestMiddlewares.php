<?php

return [
    'frontend' => [
        'mediaessenz/fullcalendar/calendar-ics' => [
            'target' => \MEDIAESSENZ\Fullcalendar\Middleware\CalendarIcsMiddleware::class,
            'before' => [
                'typo3/cms-frontend/site',
            ],
        ],
    ],
];
