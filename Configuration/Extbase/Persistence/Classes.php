<?php
declare(strict_types = 1);
return [
  \MEDIAESSENZ\Fullcalendar\Domain\Model\Category::class => [
    'tableName' => 'sys_category',
  ],
];
